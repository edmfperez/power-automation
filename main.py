# Created By: Rez
# Started: 08/12/2021
# Goal: To automate the measurements of voltage output for a devices' battery-driven power supply under load. We can use feedback recorded by this tool to gauge the performative impact that specific applications have on specific hardware.

import numpy as np
import pandas as pd
import visa
import time

# pyVISA initializers, string'd to a specific address
chromaEvLoad = visa.instrument('GPIB::2')
keysightDaq = visa.instrument('GPIB::9')

# reporting to 2-dimensional data structure; provides easy to read rows & columns
resultDataFrame = pd.DataFrame()
# variable load of output current (amperes) applied to device determined by stepping fn arrange(start, stop + step, step)
loadStepper = np.arrange(0, 20+2, 2)

# load loop
for load in loadStepper:
    # gpib instrument write methods using %.2f as a var placeholder for 'load'
    chromaEvLoad.write('CURR:STAT:L1 %.2f' % load)
    chromaEvLoad.write('LOAD ON')
    # rest to achieve circuit/instruments have reached steady-state in between tests
    time.sleep(1)

    # store each loop iteration in empty dict
    # measure output DCV w auto scale on channel @101
    # V/R = I gives us the current for measurement on channel 102
    temp = {}
    keysightDaq.write('MEAS:VOLT:DC? AUTO,DEF,(@101)')
    temp['Vout'] = float(keysightDaq.read())
    keysightDaq.write('MEAS:VOLT;DC? AUTO,DEF,(@102)')
    temp['Iout'] = float(keysightDaq.read())/0.004

    # add the dictionary into the dataframe
    resultDataFrame = resultDataFrame.append(temp, ignore_index=True)
    # testing to make sure this is still working
    print("%.2fAt%.3fV" % (temp['Iout'],temp['Vout']))

# turn off the load and save to disk
chromaEvLoad.write('LOAD OFF')

timestr = time.strftime("%Y%m%d-%H%M%S")
resultDataFrame.to_csv('test_results%d.csv' % timestr)

